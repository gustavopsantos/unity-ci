using UnityEngine;

public class NoiseRotation : MonoBehaviour
{
    [Header("General Settings")]
    public float Amplitude = 4f;
    public float Speed = 1;
    public float Frequency = 0.2f;

    [Header("Per Axis Settings")]
    public Vector3 AxisAmplitude = Vector3.one;
    public Vector3 AxisSpeed = Vector3.one;
    public Vector3 AxisFrequency = Vector3.one;

    private Vector3 _seed;
    private Vector3 _input = Vector3.zero;
    private Vector3 _initialLocalEulerAngles;

    private void Start()
    {
        _initialLocalEulerAngles = transform.localEulerAngles;

        _seed = new Vector3(
            Random.Range(-99999, 99999),
            Random.Range(-99999, 99999),
            Random.Range(-99999, 99999));
    }

    private void LateUpdate()
    {
        SetLocalEulerAngles(_initialLocalEulerAngles);
    }

    public void SetLocalEulerAngles(Vector3 localEulerAngles)
    {
        transform.localEulerAngles = GetNoise() + localEulerAngles;
    }

    private Vector3 GetNoise()
    {
        var x = (Mathf.PerlinNoise((_input.x + _seed.x) * AxisFrequency.x * Frequency, 0) * 2 - 1) * AxisAmplitude.x * Amplitude;
        var y = (Mathf.PerlinNoise((_input.y + _seed.y) * AxisFrequency.y * Frequency, 0) * 2 - 1) * AxisAmplitude.y * Amplitude;
        var z = (Mathf.PerlinNoise((_input.z + _seed.z) * AxisFrequency.z * Frequency, 0) * 2 - 1) * AxisAmplitude.z * Amplitude;

        _input.x += AxisSpeed.x * Speed * Time.deltaTime;
        _input.y += AxisSpeed.y * Speed * Time.deltaTime;
        _input.z += AxisSpeed.z * Speed * Time.deltaTime;

        return new Vector3(x, y, z);
    }
}