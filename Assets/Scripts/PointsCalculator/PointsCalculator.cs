﻿public class PointsCalculator
{
    public int CalculateTotalPoints(int killedEnemies, int killedHostages)
    {

        
        int points = 0;

        points += killedEnemies * 100;
        points -= killedHostages * 500;

        return points;
    }
}